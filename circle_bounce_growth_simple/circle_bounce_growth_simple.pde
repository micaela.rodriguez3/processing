int YSIZE = 300;

void settings(){
	size(3*YSIZE, YSIZE);
}

void setup(){
	background(255);
}

int growthrate = 5;
float r = 1.2;

int i = 1;
float p = 1;
int k = 1;

int grow1 = 1, grow3 = growthrate;
void draw(){
	int xcenter1 = (YSIZE*3)/6, xcenter2 = (YSIZE*3*3)/6, xcenter3 = (YSIZE*3*5)/6, ycenter = YSIZE/2;

	background(255);
	
	//circulo 1 (crecimiento aritmetico con rebote)
	ellipse(xcenter1, ycenter, 2*i, 2*i);
	i += grow1;
	
	if (i >= ((3*YSIZE)/8) || i <= 1)
		grow1 = -grow1;
	
	//circulo 2 (crecimiento geometrico con reset
	ellipse(xcenter2, ycenter, 2*p, 2*p);
	p *= r;

	if (p >= ((3*YSIZE)/8))
		p = 1;

	//cicrulo 3 (crecimiento aritmetico con permanencia)
	int m;
	for (m = k; m >= 1; m-= growthrate)
		ellipse(xcenter3, ycenter, 2*m, 2*m);
	k += grow3;
	
	if (k >= ((3*YSIZE)/8) || k <= 1)
		grow3 = -grow3;
}
