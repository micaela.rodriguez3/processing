int YSIZE = 300;
int radius1max = YSIZE/3;
int radius1min = 1;
int i = radius1min;
int a0 = radius1min;
float p = a0;
float r = 1.2;
int k = radius1min;
int growthrate = 5;

void settings(){
	size(3*YSIZE, YSIZE);
}

void setup(){
	background(255);
}

boolean grow1 = true;
boolean grow2 = true;
boolean grow3 = true;
void draw(){
	int xcenter1 = (YSIZE*3)/6, xcenter2 = (YSIZE*3*3)/6, xcenter3 = (YSIZE*3*5)/6, ycenter = (YSIZE/2);

	background(255);
	
	//circulo 1 (crecimiento aritmetico con rebote)
	ellipse(xcenter1, ycenter, 2*i, 2*i);
	i += (grow1) ? 1 : -1;
	
	if (i >= radius1max)
		grow1 = false;
	if (i <= radius1min)
		grow1 = true; 
	
	//circulo 2 (crecimiento geometrico con reset
	ellipse(xcenter2, ycenter, 2*p, 2*p);
	p = (grow2) ? p * r : a0;

	if (p >= radius1max)
		grow2 = false;
	if (p <= radius1min)
		grow2 = true; 

	//cicrulo 3 (crecimiento aritmetico con permanencia)
	int m;
	for (m = k; m >= radius1min; m-= growthrate)
		ellipse(xcenter3, ycenter, 2*m, 2*m);
	k += (grow3) ? growthrate : -growthrate;
	
	if (k >= radius1max)
		grow3 = false;
	if (k <= radius1min)
		grow3 = true; 
}
